This is the frame of the project.

Some hints !!!

Have a look to all the files present (most will be empty or at least incomplete), and the code examples.

Step #1:
You can run a tiny set of unittest with: python3 -m unittest discover -s project -p test_*.py -v

Step#2:
Then you can deploy the existing code in docker. ANd you can run the tiny set of deployment tests

Step#3: 
Have a look at the deployment tests, some are commented out because some of the API code is missing. 
You should work to make those tests pass, starting with test_delInitialUser() which is simple
and then test_delUser(), which will require you to code the whole /getusers route (with tests and all)

Step#4:
Ok now you are on your own... there are several direction you can take to complete the work.
To save time, you should probably start with the scripting parts so you actually use them throughout the rest of the dev process.




