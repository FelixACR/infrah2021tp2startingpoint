#Suppression
docker stop tp2facr
docker rm tp2facr
docker image rm  tp2facr_image
docker volume rm tp2facr_vol
#Création
docker volume create --name tp2facr_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2facr_image -f ./project/docker/Dockerfile .
#Execution
docker run -d -p 5555:5555 --mount source=tp2facr_vol,target=/mnt/app/ --name tp2facr tp2facr_image
docker exec -it tp2facr /bin/bash
