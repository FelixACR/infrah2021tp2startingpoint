#!/bin/bash

export PYTHONPATH=$PWD/project/
echo python path is: 
echo $PYTHONPATH 

apt update
apt --assume-yes install rsyslog
service rsyslog start

echo running the following python program 
echo $@ 

python3 "$@"
