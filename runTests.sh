python3 -m unittest discover -s project -p test_*.py -v
if [ $? -ne 0 ]
then
	echo unittest has failed, container will not launch
	exit 1
fi
sh runDocker.sh
sleep 10 #Bien que 2 secondes sont demandées ici, mes tests m'ont prouvées que 10 secondes sont nécessaires
coverage run --omit */test_*,*__init__.py,/usr/*,/home/lubuntu/.local/* -m unittest discover -s project -p dtest_*.py -v
coverage report
coverage html -d /mnt/share/coverage/html
