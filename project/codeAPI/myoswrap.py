import subprocess

def runCommandToAddUser(username):
	subprocess.run(['useradd', username])

def runCommandToRemoveUser(username):
	subprocess.run(['userdel', username, '-f', '-r'])

def readFileByLine(filename):
	content = []
	file = open(filename, 'r')
	content = file.readlines()
	return content
