import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI

class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUserWithInitialUser(self, mock_oswrap):
		actual = userAPI.isInitialUser('root')
		self.assertTrue(actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUserWithNotInitialUser(self, mock_oswrap):
		actual = userAPI.isInitialUser('Felix')
		self.assertFalse(actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserListMultipleUsers(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash', 'Felix:x:1000:1000:Felix:/home/Felix:/bin/bash', 'Antoine:x:2000:2000:Antoine:/home/Antoine:/bin/bash']
		actual = userAPI.getUserList()
		self.assertEqual(['root', 'Felix', 'Antoine'], actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogs(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['lineA', 'lineB']
		actual = userAPI.getLogs()
		self.assertEqual(['lineA', 'lineB'], actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersInitialUsers(self, mock_oswrap):
		actual = userAPI.getUsers()
		self.assertListEqual(userAPI.INITIAL_USERS, actual['InitialUsers'])
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersNoNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = []
		actual = userAPI.getUsers()
		self.assertListEqual([], actual['NewUsers'])
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersOneNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Felix:x:1000:1000:Felix:/home/Felix:/bin/bash']
		actual = userAPI.getUsers()
		self.assertListEqual(['Felix'], actual['NewUsers'])
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersOneInitialUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/root:/bin/bash']
		actual = userAPI.resetUsers()
		self.assertEqual(0, actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersOneNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Felix:x:1000:1000:Felix:/home/Felix:/bin/bash']
		actual = userAPI.resetUsers()
		self.assertEqual(1, actual)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Felix')
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = []
		userAPI.addUser('Felix')
		#mock_oswrap.runCommandToAddUser.assert_called_with('Felix')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUserAlreadyExisting(self, mock_oswrap):
		with self.assertRaises(UserAlreadyExistsException) as context:
			mock_oswrap.readFileByLine.return_value = ['Felix:x:1000:1000:Felix:/home/Felix:/bin/bash']
			userAPI.addUser('Felix')
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

if __name__ == '__main__':
	unittest.main()

